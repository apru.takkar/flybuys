//
//  FlybuysTestApp.swift
//  FlybuysTest
//
//  Created by Ashima Takkar on 30/6/2023.
//

import SwiftUI

@main
struct FlybuysTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
