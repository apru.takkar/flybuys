//
//  Constants.swift
//  FlybuysTest
//
//  Created by Ashima Takkar on 3/7/2023.
//

import Foundation
import UIKit
struct AppConstant {
    static let userPlaceholderImage = UIImage(systemName: "icloud")!
}
