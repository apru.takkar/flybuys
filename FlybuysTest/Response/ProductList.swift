//
//  ProductList.swift
//  FlybuysTest
//
//  Created by Ashima Takkar on 30/6/2023.
//

import Foundation



struct ProductList: Codable
{
   
    var limit:Int
    var products :[Products]
    var skip:Int
    var total:Int
 }
struct Products:Codable,Identifiable{
        var id: Int
        var title:String
        var description:String
        var price:Int
        var discountPercentage:Double
        var rating:Double
        var stock:Int
        var brand: String
        var category:String
        var thumbnail:String
        var images:[String]
    }

