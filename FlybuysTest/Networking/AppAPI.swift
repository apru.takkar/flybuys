//
//  AppAPI.swift
//  FlybuysTest
//
//  Created by Ashima Takkar on 30/6/2023.
//



    import Foundation
    import SwiftUI
    public typealias Parameters = [String: Any]
    public typealias HTTPHeaders = [String: String]
    struct AppAPI {
        static let shared = AppAPI()

       
        static let baseURLString = "https://dummyjson.com"
        
        
        public func GET<T: Codable>(endpoint: Endpoint, params: [String: String]? = nil, completionHandler: @escaping (Result<T, APIError>) -> Void) {
            let queryURL = endpoint.baseURL.appendingPathComponent(endpoint.path())
            
            var components = URLComponents(url: queryURL, resolvingAgainstBaseURL: true)!
            // Add params if there's any
            if let params = params {
                for (_, value) in params.enumerated() {
                    components.queryItems?.append(URLQueryItem(name: value.key, value: value.value))
                }
            }
            
            var request = URLRequest(url: components.url!)
            print(request)
            request.httpMethod = HttpMethod.GET
            
            // Add headers if there's any
            if let headers = endpoint.headers {
                for (key, value) in headers {
                    request.addValue("\(value)", forHTTPHeaderField: key)
                }
            }
            print("Headers......................", endpoint.headers)
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data else {
                    DispatchQueue.main.async {
                        completionHandler(.failure(.noResponse))
                    }
                    return
                }
                guard error == nil else {
                    DispatchQueue.main.async {
                        completionHandler(.failure(.networkError(error: error!)))
                    }
                    return
                }
                do {
                  let jsonDict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    print("Response is ............. ...", jsonDict)
                        let object = try JSONDecoder().decode(T.self, from: data)
                        DispatchQueue.main.async {
                            completionHandler(.success(object))
                        }
                 
                } catch let error {
                    DispatchQueue.main.async {
                        #if DEBUG
                        print("JSON Decoding Error: \(error)")
                        #endif
                        completionHandler(.failure(.jsonDecodingError(error: error)))
                    }
                }
            }
            task.resume()
        }
        
       
    }

    extension AppAPI {
        
        public struct HttpMethod {
            static let GET = "GET"
            
        }
        
        public enum APIError: Error {
            case noResponse
            case jsonEncodingError(error: Error)
            case jsonDecodingError(error: Error)
            case networkError(error: Error)
        }
        
        public enum Endpoint {
            case flybuysProductList
           
            
            var baseURL: URL {
                switch self {
                case .flybuysProductList:
                    guard let url = URL(string: AppAPI.baseURLString) else {fatalError("Invalid URL")}
                    return url
               
                   
                }
            }
            
            func path() -> String {
                switch self {
               
                case .flybuysProductList:
                    return "/products"
               
                }
            }
            var headers: HTTPHeaders? {
                switch self {
                    
               
                    
                case  .flybuysProductList:
                    return ["Content-Type": "application/json"]
                  
                }
            }
        }
    }


