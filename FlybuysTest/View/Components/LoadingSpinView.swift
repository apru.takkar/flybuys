//
//  LoadingSpinView.swift
//  FlybuysTest
//
//  Created by Ashima Takkar on 2/7/2023.
//


import SwiftUI

struct LoadingSpinView: View {
    var body: some View {
        ZStack {
            Color(.systemBackground)
                .ignoresSafeArea()
                .opacity(0.8)
            ProgressView()
                .progressViewStyle(CircularProgressViewStyle(tint: Color.black))
                .scaleEffect(3)
        }
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingSpinView()
    }
}
