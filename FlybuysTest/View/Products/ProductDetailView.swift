//
//  ProductDetailView.swift
//  FlybuysTest
//
//  Created by Ashima Takkar on 2/7/2023.
//

import SwiftUI

import Kingfisher


struct ProductDetailView: View {
    @State var productsDetail:Products
    var body: some View {
        VStack(alignment:.leading){
           
            HStack{
               
                Text("\(productsDetail.title)")
                    .font(Font.system(size: 16.0, weight: .bold, design: .default))
            }
            GeometryReader{ proxy in
                TabView{
                    
                    ForEach(0..<productsDetail.images.count, id: \.self){
                        image in
                         KFImage(URL(string:(productsDetail.images[image])))
                                 .placeholder {
                                     Image(uiImage: AppConstant.userPlaceholderImage)
                                         .resizable()
                                         .scaledToFill()
                                 }
                         .resizable()
                         .scaledToFill()
                         .padding(10)
                       
                    }
                }.tabViewStyle(PageTabViewStyle())
                .frame(width:proxy.size.width,height:proxy.size.height/3)}
          
                HStack{
                    Text("Brand:").font(Font.system(size: 14.0, weight: .bold, design: .default))
                    Text("\(productsDetail.brand)")
                }
                HStack{
                    Text("Category:").font(Font.system(size: 14.0, weight: .bold, design: .default))
                    Text("\(productsDetail.category)")
                }
                HStack{
                    Text("Description:").font(Font.system(size: 14.0, weight: .bold, design: .default))
                    Text("\(productsDetail.description)")
                }
                HStack{
                    Text("Disocunt Percentage:").font(Font.system(size: 14.0, weight: .bold, design: .default))
                    Text("\(productsDetail.discountPercentage)")
                }
                HStack{
                    Text("Price:").font(Font.system(size: 14.0, weight: .bold, design: .default))
                    Text("\(productsDetail.price)")
                }
                HStack{
                    Text("Stock:").font(Font.system(size: 14.0, weight: .bold, design: .default))
                    Text("\(productsDetail.stock)")
                }}.padding()
    }
}

/*struct ProductDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ProductDetailView(productsDetail: Products)
    }
}*/
