//
//  FlybuysTestListModel.swift
//  FlybuysTest
//
//  Created by Ashima Takkar on 30/6/2023.
//

import Foundation
import SwiftUI
import Combine

class FlybuysTestListModel:ObservableObject{
    @Published var flybuysTestProductList: [Products] = []
    @Published var limit:Int = 0

    @Published var skip:Int = 0
    @Published var total:Int = 0
    @Published var isLoading = false
    @Published var productDetailsView = false
 
  /*  func loadMoreProducts(currentItem item: Products){
        
        
        let thresholdIndex = self.flybuysTestProductList.index(self.flybuysTestProductList.endIndex, offsetBy: -1)
      
        self.skipPast=item.index
        skipPast = skipPast+1
        if thresholdIndex == item.index, (pastPage + 1) <= totalPages {
            
            pastPage += 1
            
            past_bookings()
        }
    }*/
   
    func refreshProductListonPull(){
        flybuysTestProductList.removeAll()
        fetchProductList()
    }
   // MARK: - Request to get RiderList.
   func fetchProductList() {
    
       self.isLoading = true
       AppAPI.shared.GET(endpoint: .flybuysProductList) { (result: Result<ProductList, AppAPI.APIError>) in
           switch result {
           case .success(let flybuysTestProductList):
            //  let _ = print(result,"dhjdjhd")
               self.flybuysTestProductList .append(contentsOf: flybuysTestProductList.products)
               self.limit=flybuysTestProductList.limit
               self.skip = flybuysTestProductList.skip
               self.total = flybuysTestProductList.total
               let _ = print(self.flybuysTestProductList,"dhjdjhd")
           case .failure(let error):
               print(error)
           }
           self.isLoading = false
       }
   }
}
