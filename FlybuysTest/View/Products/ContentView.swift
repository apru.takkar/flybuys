//
//  ContentView.swift
//  FlybuysTest
//
//  Created by Ashima Takkar on 30/6/2023.
//

import SwiftUI
import Kingfisher

struct ContentView: View {
    @StateObject private var viewModel: FlybuysTestListModel = FlybuysTestListModel()
    @State var a:Bool = false
    var body: some View {
        NavigationView {
            ZStack{
                ScrollView{
                    
                  
                    
                    LazyVStack{
                        
                        
                        ForEach(viewModel.flybuysTestProductList.indices, id: \.self) { index in
                            
                            let productsList = viewModel.flybuysTestProductList[index]
                            
                            
                            NavigationLink (destination: ProductDetailView(productsDetail:productsList)) {
                                ProductRow( thumbnail:productsList.thumbnail, title: productsList.title)
                                    .contentShape(Rectangle())
                                
                            }
                           
                            
                            
                        }.navigationTitle("Product List")
                            .navigationBarTitleDisplayMode(.inline)
                            .navigationBarBackButtonHidden(true)
                    }
                    .padding()
                    .onAppear {
                        
                        viewModel.fetchProductList()
                        
                    }
                }
                .refreshable {
                    
                    
                    
                    
                    viewModel.refreshProductListonPull()
                    
                    
                }}
            if viewModel.isLoading {
                LoadingSpinView()
            }
        }}
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
struct ProductRow: View {
    var thumbnail: String
    var title: String
    @State var isImageLoading:Bool = false
    
    var body: some View {
        HStack(spacing: 5) {
          KFImage(URL(string: thumbnail))
               .placeholder {
                    Image(uiImage: AppConstant.userPlaceholderImage)
                }
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 40.0, height: 40.0)
                .clipShape(Circle())
            if isImageLoading {
                ActivityIndicator()
            }
         
            Text(title)
            Spacer()
        }.frame(height: 50.0)
    }
}
